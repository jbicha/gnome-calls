#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# Ubuntu builds with symbolic-functions by default which causes plugin issues
# https://gitlab.gnome.org/GNOME/calls/-/issues/334
export DEB_LDFLAGS_MAINT_STRIP = -Wl,-Bsymbolic-functions

ifeq ($(filter nodoc,$(DEB_BUILD_PROFILES)),)
  CONFIGURE_OPTS+=-Dgtk_doc=true
else
  CONFIGURE_OPTS+=-Dgtk_doc=false
endif

%:
	dh $@ -- -B_build

override_dh_auto_configure:
	dh_auto_configure -- $(CONFIGURE_OPTS)

override_dh_auto_build:
	dh_auto_build
ifeq ($(filter nodoc,$(DEB_BUILD_PROFILES)),)
	# https://github.com/mesonbuild/meson/pull/2862
	dh_auto_build -- calls-doc
endif

override_dh_auto_test:
	xvfb-run dh_auto_test

# generate the manpages from the scripts using help2man
execute_before_dh_installman:
	help2man --version-option='-h' --no-info --name="A GTK+ user interface for PSTN phone calls" \
		debian/tmp/usr/bin/gnome-calls > debian/gnome-calls.1
